package com.example.asus.onwheels;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class BarangActivity extends AppCompatActivity {

    TextView tv_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang);

        tv_next = (TextView) findViewById(R.id.tv_next);
    }

    public void next(View v){
        Intent intent = new Intent(BarangActivity.this, BarangConfirmActivity.class);
        startActivity(intent);
    }
}
