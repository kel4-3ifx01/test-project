package com.example.asus.onwheels;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void daftar_click(View view){
        startActivity( new Intent(MenuActivity.this, RegisterActivity.class) );
    }

    public void komen_rating_click(View view){
        startActivity( new Intent(MenuActivity.this, FeedbackActivity.class) );
    }

    public void barang_click(View view){
        startActivity( new Intent(MenuActivity.this, BarangActivity.class) );
    }

    public void tumpangan_click(View view){
        startActivity( new Intent(MenuActivity.this, TumpanganActivity.class) );
    }

}
