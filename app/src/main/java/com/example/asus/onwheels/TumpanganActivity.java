package com.example.asus.onwheels;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class TumpanganActivity extends AppCompatActivity {

    TextView tv_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tumpangan);

        tv_next = (TextView) findViewById(R.id.tv_next);
    }

    public void nearby_click(View view){
        startActivity( new Intent(TumpanganActivity.this, NearbyActivity.class) );
    }
}
